package exercise.exercise1;

import java.util.Scanner;

public class fibonacci {
        private static int fibo(int n)
        {
            if (n<=0)
                return 0;
            if(n==1 || n==2)
                return 1;
            if(n>2) {
                return fibo(n - 1) + fibo(n - 2);
            }
            return 0;
        }
        public static void main(String[] args) {
        Scanner sr = new Scanner(System.in);
        System.out.print("请输入一个数字 : ");
        int num = sr.nextInt();
          System.out.print("斐波那契数列为 ： ");
        for(int i=1;i<=num;i++)
        {
            System.out.print(fibo(i)+" ");
        }
    }
}
